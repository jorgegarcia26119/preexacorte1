//Enrique Garcia

document.addEventListener("DOMContentLoaded", function () {
    
    // Función para buscar un vendedor por ID
    window.buscarVendedor = async function() {
      const url = "https://jsonplaceholder.typicode.com/users"; 
      const userId = document.getElementById("userId").value;
  
      try {
          const response = await axios.get(url);
          const data = response.data;
          
          const vendedor = data.find(vendedor => vendedor.id.toString() === userId);
  
          if (vendedor) {
              document.getElementById("userName").value = vendedor.name;
              document.getElementById("userUsername").value = vendedor.username;
              document.getElementById("userEmail").value = vendedor.email;
  
              // Domicilio separado por calle, número y ciudad
              const addressContainer = document.getElementById("userAddress");
              addressContainer.innerHTML = `<div><strong>Calle:</strong> ${vendedor.address.street}</div>
                                           <div><strong>Número:</strong> ${vendedor.address.suite}</div>
                                           <div><strong>Ciudad:</strong> ${vendedor.address.city}</div>`;
          } else {
              alert("No se encontró un vendedor con el ID proporcionado.");
              limpiar();
          }
      } catch (error) {
          console.error("Error en la petición:", error);
          alert("Error al realizar la petición.");
      }
    };
  
    function limpiar() {
      document.getElementById("userName").value = "";
      document.getElementById("userUsername").value = "";
      document.getElementById("userEmail").value = "";
      document.getElementById("userAddress").innerHTML = "";
    }
  });
  