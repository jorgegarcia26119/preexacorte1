function buscarPais() {
    const nombrePais = document.getElementById('nombrePais').value;

    // URL de la API Restcountries para buscar información del país por nombre
    const apiUrl = `https://restcountries.com/v2/name/${nombrePais}?fullText=true`;
    
    // Realizar la petición utilizando el objeto fetch y promesas
    fetch(apiUrl)
        .then(response => {
            if (!response.ok) {
                throw new Error('No se encontró información del país. Verifica el nombre ingresado.');
            }
            return response.json();
        })
        .then(data => {
            // Verificar si se obtuvo información válida del país
            if (data && data.length > 0) {
                const pais = data[0];
                const capital = pais.capital || 'Capital no disponible';
                const lenguajes = pais.languages ? pais.languages.map(lang => lang.name).join(', ') : 'Lenguaje no disponible';

                document.getElementById('capital').textContent = `Capital: ${capital}`;
                document.getElementById('lenguaje').textContent = `Lenguaje: ${lenguajes}`;
            } else {
                throw new Error('No se encontró información del país. Verifica el nombre ingresado.');
            }
        })
        .catch(error => {
            console.error('Error al buscar país:', error);

            // Limpiar el resultado en caso de error
            document.getElementById('capital').textContent = '';
            document.getElementById('lenguaje').textContent = `Error: ${error.message}`;
        });
}
